$(document).ready(function(){
 

$('.center').slick({
  centerMode: true,
  dots: false,
  arrows: true,
  // autoplay: true,
  speed: 300,
  autoplaySpeed: 1700,
  centerPadding: '30px',
  slidesToShow: 3,
  slidesToScroll: 2,

  responsive: [
    {
      breakpoint: 768,
      settings: {
        arrows: true,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        arrows: true,
        autoplay: true,
        centerMode: true,
        centerPadding: '25px',
        slidesToShow: 1
      }
    }
  ]
});
});