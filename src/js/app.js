   baguetteBox.run('.tz-gallery');


// new SmoothScroll();

function SmoothScroll(el) {
  var t = this, h = document.documentElement;
  el = el || window;
  t.rAF = false;
  t.target = 0;
  t.scroll = 0;
  t.animate = function() {
    t.scroll += (t.target - t.scroll) * 0.1;
    if (Math.abs(t.scroll.toFixed(5) - t.target) <= 0.47131) {
      cancelAnimationFrame(t.rAF);
      t.rAF = false;
    }
    if (el == window) scrollTo(0, t.scroll);
    else el.scrollTop = t.scroll;
    if (t.rAF) t.rAF = requestAnimationFrame(t.animate);
  };
  el.onmousewheel = function(e) {
    e.preventDefault();
    e.stopPropagation();
    var scrollEnd = (el == window) ? h.scrollHeight - h.clientHeight : el.scrollHeight - el.clientHeight;
    t.target += (e.wheelDelta > 0) ? -90 : 90;
    if (t.target < 0) t.target = 0;
    if (t.target > scrollEnd) t.target = scrollEnd;
    if (!t.rAF) t.rAF = requestAnimationFrame(t.animate);
  };
  el.onscroll = function() {
    if (t.rAF) return;
    t.target = (el == window) ? pageYOffset || h.scrollTop : el.scrollTop;
    t.scroll = t.target;
  };
}



// Select all links with hashes
$('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
      && 
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
      
    }
  });


var burger = document.getElementById('expand');
var list = document.querySelector('.navigation > ul ');


burger.addEventListener('click', function() {
	
	list.classList.toggle('show-ul');

});




// // Pirmas -----------------------------------------

var hexagon = document.getElementById('hexagon');
var circle = document.getElementById('circle');
var lorem = document.getElementById('lorem');
var number = document.getElementById('numb');

hexagon.addEventListener('mouseover', function() {
  
  circle.classList.add('darken');
  lorem.classList.add('fly-in');
  number.classList.add('getbigger');

});

hexagon.addEventListener('mouseout', function(){

  circle.classList.remove('darken');
  lorem.classList.remove('fly-in');
  number.classList.remove('getbigger');
});

// Antras -------------------------------------------

var hexagon1 = document.getElementById('hexagon1');
var circle1 = document.getElementById('circle1');
var lorem1 = document.getElementById('lorem1');
var number1 = document.getElementById('numb1');

hexagon1.addEventListener('mouseover', function() {
  
  circle1.classList.add('darken');
  lorem1.classList.add('fly-in');
  number1.classList.add('getbigger');

});

hexagon1.addEventListener('mouseout', function(){

  circle1.classList.remove('darken');
  lorem1.classList.remove('fly-in');
  number1.classList.remove('getbigger');
});

// trecias------------------------

var hexagon3 = document.getElementById('hexagon3');
var circle3 = document.getElementById('circle3');
var lorem3 = document.getElementById('lorem3');
var number3 = document.getElementById('numb3');

hexagon3.addEventListener('mouseover', function() {
  
  circle3.classList.add('darken');
  lorem3.classList.add('fly-in');
  number3.classList.add('getbigger');

});

hexagon3.addEventListener('mouseout', function(){

  circle3.classList.remove('darken');
  lorem3.classList.remove('fly-in');
  number3.classList.remove('getbigger');
});

// ketvirtas----------------

var hexagon4 = document.getElementById('hexagon4');
var circle4 = document.getElementById('circle4');
var lorem4 = document.getElementById('lorem4');
var number4 = document.getElementById('numb4');

hexagon4.addEventListener('mouseover', function() {
  
  circle4.classList.add('darken');
  lorem4.classList.add('fly-in');
  number4.classList.add('getbigger');

});

hexagon4.addEventListener('mouseout', function(){

  circle4.classList.remove('darken');
  lorem4.classList.remove('fly-in');
  number4.classList.remove('getbigger');
});



// // ---------------------------------------------------







